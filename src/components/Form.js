import React, { useState } from 'react';
import { CREATE_BLOGPOST_MUTATION } from '../GraphQL/Mutations';
import { useMutation } from '@apollo/client';

const Form = (props) => {
  const [title, setTitle] = useState("");
  const [content, setContent] =useState("");

  const [createBlogpost, { error }] = useMutation(CREATE_BLOGPOST_MUTATION);

  const addBlogpost = () => {
    createBlogpost({
      variables: {
        title: title,
        content: content,
      }
    });

    if(error) {
      console.log(error);
    }
  }

  return (
  <div>
      <p>
        <input type="text" name="title" placeholder="Title" value={title} onChange={(event) => { setTitle(event.target.value) }} />
      </p>
      <p>
        <textarea placeholder="enter the content" value={content} onChange={(event) => {setContent(event.target.value)}}></textarea>
      </p>
      <p>
        <input type="button" value="Save" onClick={addBlogpost} />
      </p>
  </div>
  );
};

export default Form;
