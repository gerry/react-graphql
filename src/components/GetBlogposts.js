import React, { useEffect, useState } from 'react';
import { useQuery } from '@apollo/client';
import { LOAD_BLOGPOSTS } from '../GraphQL/Queries';

const GetBlogposts = () => {
  const { error, loading, data } = useQuery(LOAD_BLOGPOSTS);
  const [blogposts, setBlogposts] = useState([]);

  useEffect(() => {
    if(data){
      console.log(data.blogposts.data);
      setBlogposts(data.blogposts.data);
    }
    
  }, [data])

  return (
    <div>
      {blogposts.map((blogpost) => {
        return (
          <h1 key={blogpost.id}>{blogpost.attributes.title}</h1>)
      })}
    </div>
  );
};

export default GetBlogposts;
