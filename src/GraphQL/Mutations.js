import { gql } from "@apollo/client";

export const CREATE_BLOGPOST_MUTATION = gql`
  mutation createBlogpost($title: String!, $content: String!) {
    createBlogpost(data: {title: $title, content: $content}){
      data {
        id
      }
    }
  }
`;