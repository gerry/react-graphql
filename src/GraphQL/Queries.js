import { gql } from '@apollo/client';

export const LOAD_BLOGPOSTS = gql`
  query {
    blogposts {
      data {
        id
        attributes {
          title
          content
        }
      }
    }
  }
`;